package br.erasmo.icaro.main;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import br.erasmo.icaro.map.GeoMap;
import br.erasmo.icaro.map.Node;
import br.erasmo.icaro.map.SingleRoute;

public class Executor {
	
	private GeoMap map;
	private String start;
	private String destiny;
	
	public Executor(String start, String destiny, Map<String, Integer> heuristics, List<Node>routes){
		this.start = start.trim();
		this.destiny = destiny.trim();
		map = new GeoMap();
		map.setHeuristics(heuristics);
		map.addAllRoutes(routes);
		map.addAllRoutes(routes.stream().map(item -> new Node(new SingleRoute(item.getDestiny(), item.getSource(), item.getDistance()))).collect(Collectors.toList()));
	}
	
	public Node start(){
		Node first = map.getBySourceAndBetterDistance(start, 0);
		first.setTotalDistance(first.getDistance());
		return exec(first);
	}
	
	private Node exec(Node node) {
		
		if(node.getDestiny().equalsIgnoreCase(destiny)) {
			return node;
		}
		
		Node nextNode = map.getBySourceAndBetterDistance(node.getDestiny(), node.getTotalDistance());
		
		node.setNext(nextNode);
		nextNode.setPrevious(node);
		nextNode.setTotalDistance(node.getTotalDistance() + nextNode.getDistance());
		
		return exec(nextNode);
	}
}
