package br.erasmo.icaro.main;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import br.erasmo.icaro.map.Node;
import br.erasmo.icaro.map.SingleRoute;

public class Main {

	public static void main(String[] args) {
		List<Node> routes = readDistances("distances.txt");
		Map<String, Integer> heuristics = readHeuristics("heuristics.txt");
		
		Executor exec = new Executor("Arad", "Bucharest", heuristics, routes);
		Node finalRoute = exec.start();
		System.out.println(buildRoute(finalRoute));
		System.out.println("Total Distance: "+finalRoute.getTotalDistance());
	}
	
	public static String buildRoute(Node next) {
		
		if(next.getPrevious() == null) {
			return next.getSource() +" -> "+ next.getDestiny();
		}
		
		return buildRoute(next.getPrevious())+" -> "+next.getDestiny();
	}
	
	private static List<Node> readDistances(String strFile) {

		List<Node> routes = new ArrayList<>();
		try (BufferedReader br = new BufferedReader(new FileReader(strFile))) {
		    String line;
		    while ((line = br.readLine()) != null) {
		       String[] ln = line.split(",");
		       routes.add(new Node(new SingleRoute(ln[0].trim(), ln[1].trim(), Integer.parseInt(ln[2].trim()))));
		    }
		} catch (FileNotFoundException e) {
			System.err.println("File not found");
		} catch (IOException e) {
			System.err.println("Error while reading file");
		}
		return routes;
	}
	
	private static Map<String, Integer> readHeuristics(String strFile){
		Map<String, Integer> heuristics = new HashMap<>();
		try (BufferedReader br = new BufferedReader(new FileReader(strFile))) {
		    String line;
		    while ((line = br.readLine()) != null) {
		       String[] ln = line.split(",");
		       heuristics.put(ln[0].trim(), Integer.parseInt(ln[1].trim()));
		    }
		} catch (FileNotFoundException e) {
			System.err.println("File not found");
		} catch (IOException e) {
			System.err.println("Error while reading file");
		}
		return heuristics;
	}
}
