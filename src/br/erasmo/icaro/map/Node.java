package br.erasmo.icaro.map;

public class Node {
	
	private SingleRoute singleRoute;
	
	private Node next;
	private Node previous;
	
	private Integer totalDistance;

	public Node(SingleRoute singleRoute) {
		this.setSingleRoute(singleRoute);
	}
	public SingleRoute getSingleRoute() {
		return singleRoute;
	}
	public void setSingleRoute(SingleRoute singleRoute) {
		this.singleRoute = singleRoute;
	}
	public Node getNext() {
		return next;
	}
	public void setNext(Node next) {
		this.next = next;
	}
	public void setPrevious(Node previous) {
		this.previous = previous;
	}
	public Node getPrevious() {
		return previous;
	}
	public Integer getTotalDistance() {
		return totalDistance;
	}
	public void setTotalDistance(Integer totalDistance) {
		this.totalDistance = totalDistance;
	}
	public String getSource() {
		return singleRoute.getSource();
	}
	public String getDestiny() {
		return singleRoute.getDestiny();
	}
	public Integer getDistance() {
		return singleRoute.getDistance();
	}
}
