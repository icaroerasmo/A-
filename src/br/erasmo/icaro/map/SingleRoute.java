package br.erasmo.icaro.map;

public class SingleRoute {
	
	private String source;
	private String destiny;
	private Integer distance;

	public SingleRoute(String source, String destiny, Integer distance) {
		this.source = source;
		this.destiny = destiny;
		this.distance = distance;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getDestiny() {
		return destiny;
	}

	public void setDestiny(String destiny) {
		this.destiny = destiny;
	}

	public Integer getDistance() {
		return distance;
	}

	public void setDistance(Integer distance) {
		this.distance = distance;
	}
}