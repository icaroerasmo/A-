package br.erasmo.icaro.map;

import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

public class GeoMap {
	
	private Set<Node> routes;
	private Map<String, Integer> heuristics;
	
	public GeoMap(){
		routes = new HashSet<>();
	}
	
	public void addRoute(Node route){
		routes.add(route);
	}
	
	public void addAllRoutes(Node ...routes) {
		for(Node route : routes) {
			this.routes.add(route);
		}
	}
	
	public void addAllRoutes(List<Node> routes) {
		this.routes.addAll(routes);
	}
	
	public void removeRoute(Node route) {
		this.routes.remove(route);
	}
	
	public void setHeuristics(Map<String, Integer> heuristics) {
		this.heuristics = heuristics;
	}
	
	public Node getBySourceAndDestiny(String source, String destiny) {
		Optional<Node> node = routes.stream().filter(
				item -> item.getSource().equalsIgnoreCase(source) && 
				item.getDestiny().equalsIgnoreCase(destiny)).findFirst();
		return node.isPresent() ? node.get() : null;
	}
	
	public List<Node> getBySource(String source) {
		return routes.stream().filter(
				item -> item.getSource()
				.equalsIgnoreCase(source))
				.collect(Collectors.toList());
	}
	
	public Node getBySourceAndBetterDistance(String source, Integer ranDistance) {
		Optional<Node> route = routes.stream()
				.filter(item -> item.getSource().equalsIgnoreCase(source))
				.min(Comparator.comparing(
						item -> ranDistance + item.getDistance() + (heuristics.get(item.getDestiny()) != 
						null ? heuristics.get(item.getDestiny()) : 0)
						));
		
		return route.isPresent() ? route.get() : null;
	}
}
